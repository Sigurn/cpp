package cpp

import (
	"reflect"
	"testing"
)

func TestDestructor(t *testing.T) {
	dtor := CreateDestructor()
	expected := []string{"~();"}
	actual := dtor.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("dtor.Header(4) = %v != %v", actual, expected)
	}
}

func TestDestructorInClass(t *testing.T) {
	class := CreateClass("Class1")
	dtor := CreateDestructor()
	class.Public(dtor)
	expected := []string{"~Class1();"}
	actual := dtor.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("dtor.Header(4) = %v != %v", actual, expected)
	}
}

func TestVirtualDestructorInClass(t *testing.T) {
	class := CreateClass("Class1")
	dtor := CreateDestructor().Virtual(true)
	class.Public(dtor)
	expected := []string{"virtual ~Class1();"}
	actual := dtor.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("dtor.Header(4) = %v != %v", actual, expected)
	}
}

func TestVirtualDestructorSource(t *testing.T) {
	class := CreateClass("Class1")
	dtor := CreateDestructor().Virtual(true)
	class.Public(dtor)
	dtor.Code(
		Scope("if (m_data != null)").Code(
			Line("delete m_data;"),
			Line("m_data = null;"),
		),
	)
	expected := []string{
		"Class1::~Class1()",
		"{",
		"    if (m_data != null)",
		"    {",
		"        delete m_data;",
		"        m_data = null;",
		"    }",
		"}",
	}
	actual := dtor.Source(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("dtor.Source(4) = %v != %v", actual, expected)
	}
}
