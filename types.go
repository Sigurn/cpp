package cpp

import "fmt"

type Named interface {
	Name(fullName bool) string
}

type Entity interface {
	Named
	Parent() Entity
	Header(tabSize uint8) []string
	Source(tabSize uint8) []string
	SetParent(parent Entity)
}

type CodeEntity interface {
	Source(tabSize uint8) []string
}

type Value interface {
	fmt.Stringer
}

type lazyValue struct {
	resolver func() string
}

func (lv *lazyValue) String() string {
	return lv.resolver()
}

func CreateLazyValue(resolver func() string) Value {
	return &lazyValue{resolver}
}

type Type interface {
	fmt.Stringer
	Named
	DefaultValue() Value
}

type typeInfo struct {
	name         string
	defaultValue Value
}

type lazyType struct {
	resolve func(fullName bool) string
}

func (lt *lazyType) String() string {
	return lt.resolve(false)
}

func (lt *lazyType) Name(fullName bool) string {
	return lt.resolve(fullName)
}

func (lt *lazyType) DefaultValue() Value {
	return CreateValue("")
}

func CreateLazyType(resolver func(fullName bool) string) Type {
	return &lazyType{resolve: resolver}
}

type vector struct {
	itemType Type
}

func CreateVectorType(itemType Type) Type {
	return &vector{itemType}
}

func (v *vector) String() string {
	return v.Name(false)
}

func (v *vector) Name(fullName bool) string {
	return fmt.Sprintf("std::vector<%s>", v.itemType.Name(fullName))
}

func (v *vector) DefaultValue() Value {
	return CreateValue("")
}

func CreateType(name string) Type {
	return &typeInfo{name: name, defaultValue: Empty()}
}

func CreateTypeWithDefaultValue(name string, defValue Value) Type {
	return &typeInfo{name: name, defaultValue: defValue}
}

func (ti *typeInfo) Name(fullName bool) string {
	return ti.name
}

func (ti *typeInfo) DefaultValue() Value {
	return ti.defaultValue
}

func (ti *typeInfo) String() string {
	return ti.Name(true)
}

type typeModifier struct {
	baseType Type
	template string
}

func (tm *typeModifier) Name(fullName bool) string {
	return fmt.Sprintf(tm.template, tm.baseType.Name(fullName))
}

func (tm *typeModifier) DefaultValue() Value {
	return tm.baseType.DefaultValue()
}

func (tm *typeModifier) String() string {
	return tm.Name(true)
}

func Const(baseType Type) Type {
	return &typeModifier{baseType: baseType, template: "const %s"}
}

func Ref(baseType Type) Type {
	return &typeModifier{baseType: baseType, template: "%s&"}
}

func ConstRef(baseType Type) Type {
	return &typeModifier{baseType: baseType, template: "const %s&"}
}

type customValue struct {
	value string
}

func CreateValue(value string) Value {
	return &customValue{value}
}

func (cv *customValue) String() string {
	return cv.value
}

func Empty() Value {
	return CreateValue("")
}

func Void() Value {
	return CreateValue("void")
}

func Bool(value bool) Value {
	return CreateValue(fmt.Sprint(value))
}

func Int8(value int8) Value {
	return CreateValue(fmt.Sprint(value))
}

func Int16(value int16) Value {
	return CreateValue(fmt.Sprint(value))
}

func Int32(value int32) Value {
	return CreateValue(fmt.Sprint(value))
}

func Int64(value int64) Value {
	return CreateValue(fmt.Sprint(value))
}

func UInt8(value uint8) Value {
	return CreateValue(fmt.Sprint(value))
}

func UInt16(value uint16) Value {
	return CreateValue(fmt.Sprint(value))
}

func UInt32(value uint32) Value {
	return CreateValue(fmt.Sprint(value))
}

func UInt64(value uint64) Value {
	return CreateValue(fmt.Sprint(value))
}

func Float(value float32) Value {
	return CreateValue(fmt.Sprint(value))
}

func Double(value float64) Value {
	return CreateValue(fmt.Sprint(value))
}

func String(value string) Value {
	return CreateValue(fmt.Sprintf("L\"%v\"", value))
}

var (
	VoidType   = CreateTypeWithDefaultValue("void", Void())
	BoolType   = CreateTypeWithDefaultValue("bool", Bool(false))
	Int8Type   = CreateTypeWithDefaultValue("int8_t", Int8(0))
	Int16Type  = CreateTypeWithDefaultValue("int16_t", Int16(0))
	Int32Type  = CreateTypeWithDefaultValue("int32_t", Int32(0))
	Int64Type  = CreateTypeWithDefaultValue("int64_t", Int64(0))
	UInt8Type  = CreateTypeWithDefaultValue("uint8_t", UInt8(0))
	UInt16Type = CreateTypeWithDefaultValue("uint16_t", UInt16(0))
	UInt32Type = CreateTypeWithDefaultValue("uint32_t", UInt32(0))
	UInt64Type = CreateTypeWithDefaultValue("uint64_t", UInt64(0))
	FloatType  = CreateTypeWithDefaultValue("float", Float(0.0))
	DoubleType = CreateTypeWithDefaultValue("double", Double(0.0))
	StringType = CreateTypeWithDefaultValue("std::wstring", String(""))
)
