package cpp

import (
	"bytes"
	"fmt"
	"strings"
)

const (
	Public    = iota
	Protected = iota
	Private   = iota
)

type MemberGroup struct {
	protection string
	entities   []Entity
	parent     Entity
}

func CreateGroup() *MemberGroup {
	return &MemberGroup{protection: "", entities: make([]Entity, 0), parent: nil}
}

func createGroupWithName(name string) *MemberGroup {
	return &MemberGroup{protection: name, entities: make([]Entity, 0), parent: nil}
}

func (mg *MemberGroup) Name(fullName bool) string {
	if mg.Parent() != nil {
		return mg.Parent().Name(fullName)
	}
	return ""
}

func (mg *MemberGroup) Parent() Entity {
	return mg.parent
}

func (mg *MemberGroup) Header(tabSize uint8) []string {
	header := make([]string, 0)
	if mg.protection == "" {
		header = append(header, "")
		for _, entity := range mg.entities {
			header = append(header, entity.Header(tabSize)...)
		}
	} else {
		indent := strings.Repeat(" ", int(tabSize))
		header = append(header, fmt.Sprintf("%s:", mg.protection))
		for _, entity := range mg.entities {
			for _, s := range entity.Header(tabSize) {
				header = append(header, fmt.Sprintf("%s%s", indent, s))
			}
		}
	}
	return header
}

func (mg *MemberGroup) Source(tabSize uint8) []string {
	source := make([]string, 0)
	for n, entity := range mg.entities {
		switch entity.(type) {
		case *Constructor, *Destructor, *Namespace, *Class, *MemberGroup:
		case *Method:
			if method, ok := entity.(*Method); ok && method.IsTemplate() {
				continue
			}
		default:
			continue
		}
		if n != 0 {
			source = append(source, "", "")
		}
		source = append(source, entity.Source(tabSize)...)
	}
	return source
}

func (mg *MemberGroup) SetParent(parent Entity) {
	mg.parent = parent
}

func (mg *MemberGroup) Add(entities ...Entity) *MemberGroup {
	for _, e := range entities {
		mg.entities = append(mg.entities, e)
		e.SetParent(mg)
	}
	return mg
}

type Friend struct {
	friend Type
	parent Entity
}

func CreateFriend(cppType Type) *Friend {
	return &Friend{friend: cppType, parent: nil}
}

func (fr *Friend) Name(fullName bool) string {
	return ""
}

func (fr *Friend) Parent() Entity {
	return fr.parent
}

func (fr *Friend) Header(tabSize uint8) []string {
	return []string{fmt.Sprintf("friend %s;", fr.friend.Name(true))}
}

func (fr *Friend) Source(tabSize uint8) []string {
	return nil
}

func (fr *Friend) SetParent(parent Entity) {
	fr.parent = parent
}

type inheritList struct {
	public    []Type
	protected []Type
	private   []Type
}

type Class struct {
	isStruct      bool
	name          string
	parent        Entity
	declareParams []Argument
	useParams     []Type
	members       []*MemberGroup
	inheritance   inheritList
}

func CreateClass(name string) *Class {
	return &Class{name: name, isStruct: false}
}

func CreateStruct(name string) *Class {
	return &Class{name: name, isStruct: true}
}

func (cls *Class) String() string {
	return cls.Name(false)
}

func (cls *Class) DefaultValue() Value {
	return CreateValue("")
}

func (cls *Class) Name(fullName bool) string {
	if !fullName || cls.Parent() == nil {
		return cls.name
	}
	var buffer bytes.Buffer
	buffer.WriteString(fmt.Sprintf("%s::%s", cls.Parent().Name(fullName), cls.name))
	if len(cls.useParams) != 0 {
		buffer.WriteString("<")
		for n, param := range cls.useParams {
			if n != 0 {
				buffer.WriteString(", ")
			}
			buffer.WriteString(param.Name(fullName))
		}
		buffer.WriteString(">")
	}
	return buffer.String()
}

func (cls *Class) Parent() Entity {
	return cls.parent
}

func headerInheritance(tabSize uint8, inheritance *inheritList) []string {
	indent := strings.Repeat(" ", int(tabSize))
	header := make([]string, 0)
	for _, item := range inheritance.public {
		if len(header) == 0 {
			header = append(header, fmt.Sprintf("%s: public %s", indent, item.Name(true)))
		} else {
			header = append(header, fmt.Sprintf("%s, public %s", indent, item.Name(true)))
		}
	}
	for _, item := range inheritance.protected {
		if len(header) == 0 {
			header = append(header, fmt.Sprintf("%s: protected %s", indent, item.Name(true)))
		} else {
			header = append(header, fmt.Sprintf("%s, protected %s", indent, item.Name(true)))
		}
	}
	for _, item := range inheritance.private {
		if len(header) == 0 {
			header = append(header, fmt.Sprintf("%s: private %s", indent, item.Name(true)))
		} else {
			header = append(header, fmt.Sprintf("%s, private %s", indent, item.Name(true)))
		}
	}
	return header
}

func templateParams(params []Argument) []string {
	var buffer bytes.Buffer
	buffer.WriteString("template <")
	for n, param := range params {
		if n != 0 {
			buffer.WriteString(", ")
		}
		buffer.WriteString(param.SourceString())
	}
	buffer.WriteString(">")
	return []string{buffer.String()}
}

func (cls *Class) Header(tabSize uint8) []string {
	header := make([]string, 0)
	if len(cls.declareParams) != 0 || len(cls.useParams) != 0 {
		header = append(header, templateParams(cls.declareParams)...)
	}

	var buffer bytes.Buffer
	if cls.isStruct {
		buffer.WriteString("struct")
	} else {
		buffer.WriteString("class")
	}

	if cls.name != "" {
		buffer.WriteString(fmt.Sprintf(" %s", cls.name))
	}

	if len(cls.useParams) != 0 {
		buffer.WriteString("<")
		for n, param := range cls.useParams {
			if n != 0 {
				buffer.WriteString(", ")
			}
			buffer.WriteString(param.Name(true))
		}
		buffer.WriteString(">")
	}

	header = append(header, buffer.String())
	header = append(header, headerInheritance(tabSize, &cls.inheritance)...)
	header = append(header, "{")
	for n, m := range cls.members {
		if n != 0 {
			header = append(header, "")
		}
		header = append(header, m.Header(tabSize)...)
	}
	header = append(header, "};")
	return header
}

func (cls *Class) Source(tabSize uint8) []string {
	source := make([]string, 0)
	for n, m := range cls.members {
		sources := m.Source(tabSize)
		if n != 0 && len(sources) != 0 {
			source = append(source, "", "")
		}
		source = append(source, sources...)
	}
	return source
}

func (cls *Class) SetParent(parent Entity) {
	cls.parent = parent
}

func (cls *Class) Public(entities ...Entity) *Class {
	group := createGroupWithName("public")
	group.SetParent(cls)
	group.Add(entities...)
	cls.members = append(cls.members, group)
	return cls
}

func (cls *Class) Protected(entities ...Entity) *Class {
	group := createGroupWithName("protected")
	group.SetParent(cls)
	group.Add(entities...)
	cls.members = append(cls.members, group)
	return cls
}

func (cls *Class) Private(entities ...Entity) *Class {
	group := createGroupWithName("private")
	group.SetParent(cls)
	group.Add(entities...)
	cls.members = append(cls.members, group)
	return cls
}

func (cls *Class) Inherit(protection int, types ...Type) *Class {
	for _, t := range types {
		switch protection {
		case Public:
			cls.inheritance.public = append(cls.inheritance.public, t)
		case Protected:
			cls.inheritance.protected = append(cls.inheritance.protected, t)
		case Private:
			cls.inheritance.private = append(cls.inheritance.private, t)
		}
	}
	return cls
}

func (cls *Class) DeclareParameters(params ...Argument) *Class {
	cls.declareParams = append(cls.declareParams, params...)
	return cls
}

func (cls *Class) UseParameters(params ...Type) *Class {
	cls.useParams = append(cls.useParams, params...)
	return cls
}
