package cpp

import (
	"fmt"
	"strings"
)

type EnumItem struct {
	parent *Enum
	name   string
	value  Value
}

func (ei EnumItem) String() string {
	return fmt.Sprintf("%s::%s", ei.parent.Name(true), ei.name)
}

type Enum struct {
	name         string
	parent       Entity
	baseType     Type
	items        []EnumItem
	defaultValue Value
	meta         map[string]interface{}
}

func CreateEnumWithBaseType(name string, baseType Type) *Enum {
	return &Enum{name: name, baseType: baseType, meta: make(map[string]interface{})}
}

func CreateEnum(name string) *Enum {
	return CreateEnumWithBaseType(name, nil)
}

func (e *Enum) String() string {
	return e.Name(false)
}

func (e *Enum) Name(fullName bool) string {
	if !fullName || e.Parent() == nil {
		return e.name
	}
	return fmt.Sprintf("%s::%s", e.Parent().Name(fullName), e.name)
}

func (e *Enum) Parent() Entity {
	return e.parent
}

func (e *Enum) Header(tabSize uint8) []string {
	if e.name == "" {
		return nil
	}
	header := make([]string, 0)
	if e.baseType == nil {
		header = append(header, fmt.Sprintf("enum class %s", e.name))
	} else {
		header = append(header, fmt.Sprintf("enum class %s : %s", e.name, e.baseType.Name(true)))
	}
	indent := strings.Repeat(" ", int(tabSize))
	header = append(header, "{")
	for _, item := range e.items {
		if item.value != nil {
			header = append(header, fmt.Sprintf("%s%s = %s,", indent, item.name, item.value))
		} else {
			header = append(header, fmt.Sprintf("%s%s,", indent, item.name))
		}
	}
	header = append(header, "};")
	return header
}

func (e *Enum) Source(tabSize uint8) []string {
	return nil
}

func (e *Enum) SetParent(parent Entity) {
	e.parent = parent
}

func (e *Enum) DefaultValue() Value {
	if len(e.items) == 0 {
		return CreateValue("")
	}
	return e.items[0]
}

func (e *Enum) AddItemWithValue(name string, value Value) *Enum {
	e.items = append(e.items, EnumItem{parent: e, name: name, value: value})
	return e
}

func (e *Enum) AddItem(name string) *Enum {
	return e.AddItemWithValue(name, nil)
}

func (e *Enum) BaseType() Type {
	if e.baseType == nil {
		return Int32Type
	}
	return e.baseType
}

func (e *Enum) GetMeta(name string) (interface{}, bool) {
	val, ok := e.meta[name]
	return val, ok
}

func (e *Enum) SetMeta(name string, value interface{}) {
	e.meta[name] = value
}
