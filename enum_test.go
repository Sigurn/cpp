package cpp

import (
	"reflect"
	"testing"
)

func TestCreateEnum(t *testing.T) {
	enum := CreateEnum("")
	name := enum.Name(true)
	if name != "" {
		t.Errorf("enum.Name(true) = %v != ''", name)
	}
	parent := enum.Parent()
	if parent != nil {
		t.Errorf("enum.Parent() = %v != nil", parent)
	}
	header := enum.Header(4)
	if header != nil {
		t.Errorf("enum.Header(4) = %v != nil", header)
	}
	source := enum.Source(4)
	if source != nil {
		t.Errorf("enum.Source(4) = %v != nil", source)
	}
}

func TestCreateEnumWithBaseType(t *testing.T) {
	enum := CreateEnumWithBaseType("TestEnum", UInt8Type)
	expected := []string{
		"enum class TestEnum : uint8_t",
		"{",
		"};",
	}
	actual := enum.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("enum.Header(4) = '%v' != '%v'", actual, expected)
	}
}

func TestCreateEnumWithItems(t *testing.T) {
	enum := CreateEnum("TestEnum")
	enum.AddItem("Item1")
	enum.AddItem("Item2")
	enum.AddItem("Item3")
	expected := []string{
		"enum class TestEnum",
		"{",
		"    Item1,",
		"    Item2,",
		"    Item3,",
		"};",
	}
	actual := enum.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("enum.Header(4) = '%v' != '%v'", actual, expected)
	}
}

func TestCreateEnumWithItemsAndValues(t *testing.T) {
	enum := CreateEnumWithBaseType("TestEnum", UInt8Type)
	enum.AddItemWithValue("Item1", UInt8(45))
	enum.AddItemWithValue("Item2", UInt8(124))
	enum.AddItemWithValue("Item3", UInt8(32))
	expected := []string{
		"enum class TestEnum : uint8_t",
		"{",
		"    Item1 = 45,",
		"    Item2 = 124,",
		"    Item3 = 32,",
		"};",
	}
	actual := enum.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("enum.Header(4) = '%v' != '%v'", actual, expected)
	}
}

func TestEnumDefaultValueForEmptyEnum(t *testing.T) {
	enum := CreateEnum("")
	expected := ""
	actual := enum.DefaultValue().String()
	if actual != expected {
		t.Errorf("enum.DefaultValue() = '%v' != '%v'", actual, expected)
	}
}

func TestEnumDefaultValueWhenEnumDesntHaveItems(t *testing.T) {
	enum := CreateEnumWithBaseType("TestEnum", UInt8Type)
	expected := ""
	actual := enum.DefaultValue().String()
	if actual != expected {
		t.Errorf("enum.DefaultValue() = '%v' != '%v'", actual, expected)
	}
}

func TestEnumDefaultValue(t *testing.T) {
	enum := CreateEnum("TestEnum")
	enum.AddItemWithValue("Item1", UInt8(45))
	enum.AddItemWithValue("Item2", UInt8(124))
	enum.AddItemWithValue("Item3", UInt8(32))
	expected := "TestEnum::Item1"
	actual := enum.DefaultValue().String()
	if actual != expected {
		t.Errorf("enum.DefaultValue() = '%v' != '%v'", actual, expected)
	}
}

func TestEnumDefaultValueWithParent(t *testing.T) {
	enum := CreateEnum("TestEnum").AddItem("Value1")
	CreateNamespace("unit").AddChildren(enum)
	expected := "unit::TestEnum::Value1"
	actual := enum.DefaultValue().String()
	if actual != expected {
		t.Errorf("enum.DefaultValue() = '%v' != '%v'", actual, expected)
	}
}

func TestEnumName(t *testing.T) {
	enum := CreateEnum("TestEnum").AddItem("Value1")
	CreateNamespace("unit").AddChildren(enum)
	expected := "TestEnum"
	actual := enum.Name(false)
	if actual != expected {
		t.Errorf("enum.Name(false) = '%v' != %v", actual, expected)
	}
	expected = "unit::TestEnum"
	actual = enum.Name(true)
	if actual != expected {
		t.Errorf("enum.Name(true) = '%v' != %v", actual, expected)
	}
}

func TestEnumWithParent(t *testing.T) {
	ns := CreateNamespace("unit").AddChildren(
		CreateEnum("TestEnum").AddItem("Value1").AddItem("Value2"))
	expected := []string{
		"namespace unit",
		"{",
		"    enum class TestEnum",
		"    {",
		"        Value1,",
		"        Value2,",
		"    };",
		"}",
	}
	actual := ns.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("ns.Header(4) = '%v' != %v", actual, expected)
	}
}
