package cpp

import (
	"reflect"
	"testing"
)

func TestFieldHeader(t *testing.T) {
	field := CreateField("m_test", Int32Type)
	expected := []string{"int32_t m_test;"}
	actual := field.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("field.Header(4) = %v != %v", actual, expected)
	}
}
