package cpp

import "testing"
import "reflect"

func TestUnnamedMethod(t *testing.T) {
	method := CreateMethod("")

	if method.Parent() != nil {
		t.Errorf("method.Parent() != nil")
	}

	expectedName := ""
	name := method.Name(false)
	if name != expectedName {
		t.Errorf("method.Name(false) = '%v' != '%v'", name, expectedName)
	}

	expectedFullName := ""
	fullName := method.Name(true)
	if fullName != expectedFullName {
		t.Errorf("method.Name(true) = '%v' != '%v'", fullName, expectedFullName)
	}
}

func TestGlobalMethod(t *testing.T) {
	method := CreateMethod("TestMethod")

	expectedName := "TestMethod"
	name := method.Name(false)
	if name != expectedName {
		t.Errorf("method.Name(false) = '%v' != '%v'", name, expectedName)
	}

	expectedFullName := "TestMethod"
	fullName := method.Name(true)
	if fullName != expectedFullName {
		t.Errorf("method.Name(true) = '%v' != '%v'", fullName, expectedFullName)
	}
}

func TestMethodWithParent(t *testing.T) {
	method := CreateMethod("Method")
	CreateNamespace("test").AddChildren(method)
	expectedName := "Method"
	name := method.Name(false)
	if name != expectedName {
		t.Errorf("method.Name(false) = '%v' != '%v'", name, expectedName)
	}

	expectedFullName := "test::Method"
	fullName := method.Name(true)
	if fullName != expectedFullName {
		t.Errorf("method.Name(true) = '%v' != '%v'", fullName, expectedFullName)
	}
}

func TestMethodWithArguments(t *testing.T) {
	method := CreateMethodWithRetTypeAndArgs("TestMethod", BoolType)
	method.Arguments(CreateArgument("arg1", StringType), CreateArgument("arg2", BoolType))
	expected := []string{"bool TestMethod(std::wstring arg1, bool arg2);"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
}

func TestMethodWithCallingConvention(t *testing.T) {
	method := CreateMethodWithRetTypeAndArgs("TestMethod", BoolType).
		CallingConvention("__stdcall").
		Arguments(CreateArgument("arg1", Int32Type), CreateArgument("arg2", BoolType))
	expected := []string{"bool __stdcall TestMethod(int32_t arg1, bool arg2);"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
}

func TestGlobaStaticMethod(t *testing.T) {
	method := CreateMethod("TestMethod").Static(true)
	expected := []string{"void TestMethod();"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
}

func TestMemberMethod(t *testing.T) {
	method := CreateMethod("TestMethod")
	CreateClass("TestClass").Public(method)
	expected := []string{"void TestMethod();"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
	expectedFullName := "TestClass::TestMethod"
	actualFullName := method.Name(true)
	if !reflect.DeepEqual(actualFullName, expectedFullName) {
		t.Errorf("method.Name(true) = %v != %v", actualFullName, expectedFullName)
	}
}

func TestMemberStaticMethod(t *testing.T) {
	method := CreateMethod("TestMethod").Static(true)
	CreateClass("TestClass").Public(method)
	expected := []string{"static void TestMethod();"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
}

func TestMemberVirtualMethod(t *testing.T) {
	method := CreateMethod("TestMethod").Virtual(true)
	CreateClass("TestClass").Public(method)
	expected := []string{"virtual void TestMethod();"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
}

func TestMemberConstMethod(t *testing.T) {
	method := CreateMethod("TestMethod").Const(true)
	CreateClass("TestClass").Public(method)
	expected := []string{"void TestMethod() const;"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
}

func TestMemberPureVirtualMethod(t *testing.T) {
	method := CreateMethod("TestMethod").PureVirtual(true)
	CreateClass("TestClass").Public(method)
	expected := []string{"virtual void TestMethod() = 0;"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
}

func TestMemberVirtualConstMethod(t *testing.T) {
	method := CreateMethod("TestMethod").Virtual(true).Const(true)
	CreateClass("TestClass").Public(method)
	expected := []string{"virtual void TestMethod() const;"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
}

func TestMemberPureVirtualConstMethod(t *testing.T) {
	method := CreateMethod("TestMethod").PureVirtual(true).Const(true)
	CreateClass("TestClass").Protected(method)
	expected := []string{"virtual void TestMethod() const = 0;"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
}

func TestMemberStaticPureVirtualConstMethod(t *testing.T) {
	method := CreateMethod("TestMethod").Static(true).PureVirtual(true).Const(true)
	CreateClass("TestClass").Private(method)
	expected := []string{"static void TestMethod();"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
}

func TestPureVirtualConstMethodInNamespace(t *testing.T) {
	method := CreateMethod("TestMethod").Static(true).PureVirtual(true).Const(true)
	CreateNamespace("test").AddChildren(method)
	expected := []string{"void TestMethod();"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
	expectedFullName := "test::TestMethod"
	actualFullName := method.Name(true)
	if !reflect.DeepEqual(actualFullName, expectedFullName) {
		t.Errorf("method.Name(true) = %v != %v", actualFullName, expectedFullName)
	}
}

func TestTemplateMethod(t *testing.T) {
	method := CreateMethod("TestMethod").Template(true)
	CreateNamespace("test").AddChildren(method)
	expectedHeader := []string{"template<>", "void TestMethod();"}
	actualHeader := method.Header(4)
	if !reflect.DeepEqual(actualHeader, expectedHeader) {
		t.Errorf("method.Header(4) = %v != %v", actualHeader, expectedHeader)
	}
	expectedSource := []string{"template<>", "void test::TestMethod()", "{", "}"}
	actualSource := method.Source(4)
	if !reflect.DeepEqual(actualSource, expectedSource) {
		t.Errorf("method.Source(4) = %v != %v", actualSource, expectedSource)
	}
}

func TestTemplateMethodWithParameters(t *testing.T) {
	method := CreateMethod("TestMethod").Return(CreateType("R")).
		DeclareParameters(
			CreateArgument("T", CreateType("typename")),
			CreateArgument("R", CreateType("typename"))).
		Arguments(
			CreateArgument("arg", CreateType("T")))
	CreateNamespace("test").AddChildren(method)
	expected := []string{"template<typename T, typename R>", "R TestMethod(T arg)", "{", "}"}
	actual := method.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Header(4) = %v != %v", actual, expected)
	}
	expectedFullName := "test::TestMethod"
	actualFullName := method.Name(true)
	if !reflect.DeepEqual(actualFullName, expectedFullName) {
		t.Errorf("method.Name(true) = %v != %v", actualFullName, expectedFullName)
	}
}

func TestSimpleMethodSource(t *testing.T) {
	method := CreateMethod("SimpleMethod").
		Arguments(CreateArgumentWithDefaultValue("arg", ConstRef(StringType), String("Test")))
	CreateNamespace("test").AddChildren(method)
	method.Code(
		Line("std::cout << arg << std::endl;"),
		Scope("if (arg == L\"\")").Code(
			Line("throw std::invalid_argument();"),
		),
	)
	expected := []string{
		"void test::SimpleMethod(const std::wstring& arg)",
		"{",
		"    std::cout << arg << std::endl;",
		"    if (arg == L\"\")",
		"    {",
		"        throw std::invalid_argument();",
		"    }",
		"}"}
	actual := method.Source(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Source(4) = %v != %v", actual, expected)
	}
}

func TestConstVirtualClassMethodSource(t *testing.T) {
	method := CreateMethod("ConstVirtualClassMethod").Const(true).Virtual(true).
		Arguments(CreateArgumentWithDefaultValue("arg", ConstRef(StringType), String("Test")))
	CreateClass("TestClass").Private(method)
	if method.Parent() == nil {
		t.Errorf("method.Parent() == nil")
	}
	method.Code(
		Line("std::cout << arg << std::endl;"),
		Scope("if (arg == L\"\")").Code(
			Line("throw std::invalid_argument();"),
		),
	)
	expected := []string{
		"void TestClass::ConstVirtualClassMethod(const std::wstring& arg) const",
		"{",
		"    std::cout << arg << std::endl;",
		"    if (arg == L\"\")",
		"    {",
		"        throw std::invalid_argument();",
		"    }",
		"}"}
	actual := method.Source(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("method.Source(4) = %v != %v", actual, expected)
	}
}
