package cpp

import (
	"reflect"
	"testing"
)

func TestConstructor(t *testing.T) {
	ctor := CreateConstructor()
	if ctor.Name(false) != "" {
		t.Error("ctor.Name(false) != ''")
	}
	expected := []string{"();"}
	actual := ctor.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("ctor.Header(4) = %v != %v", actual, expected)
	}
}

func TestEmptyConstructor(t *testing.T) {
	class := CreateClass("TestClass")
	ctor := CreateConstructor()
	class.Public(ctor)
	expected := []string{"TestClass();"}
	actual := ctor.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("ctor.Header(4) = %v != %v", actual, expected)
	}
}

func TestSingleArgConstructor(t *testing.T) {
	class := CreateClass("TestClass")
	ctor := CreateConstructor(CreateArgument("name", StringType))
	class.Public(ctor)
	expected := []string{"explicit TestClass(std::wstring name);"}
	actual := ctor.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("ctor.Header(4) = %v != %v", actual, expected)
	}
}

func TestMultiArgConstructor(t *testing.T) {
	class := CreateClass("TestClass")
	ctor := CreateConstructor().Arguments(
		CreateArgument("name", StringType),
		CreateArgumentWithDefaultValue("flag", BoolType, Bool(true))).Explicit(true)
	class.Public(ctor)
	expected := []string{"explicit TestClass(std::wstring name, bool flag = true);"}
	actual := ctor.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("ctor.Header(4) = %v != %v", actual, expected)
	}
}

func TestConstructorInitList(t *testing.T) {
	ctor := CreateConstructor().Arguments(CreateArgument("name", ConstRef(StringType))).
		Init(
			CreateInitField("m_field1", Int32(31415927)),
			CreateInitField("m_field2", CreateValue("name")),
			CreateInitField("m_field3", Bool(true)),
		).Code(
		Line("throw std::exception(\"Not implemented\");"),
	)
	CreateClass("FooClass").Public(ctor)
	expected := []string{
		"FooClass::FooClass(const std::wstring& name)",
		"    : m_field1(31415927)",
		"    , m_field2(name)",
		"    , m_field3(true)",
		"{",
		"    throw std::exception(\"Not implemented\");",
		"}",
	}
	actual := ctor.Source(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("ctor.Source(4) = %v != %v", actual, expected)
	}

}
