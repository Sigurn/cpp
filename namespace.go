package cpp

import "fmt"
import "strings"

type Namespace struct {
	name     string
	parent   Entity
	children []Entity
}

func CreateNamespaceWithParent(name string, parent Entity) *Namespace {
	return &Namespace{name: name, parent: parent}
}

func CreateNamespace(name string) *Namespace {
	return CreateNamespaceWithParent(name, nil)
}

func (ns *Namespace) Name(fullName bool) string {
	if !fullName || ns.Parent() == nil {
		return ns.name
	}
	return fmt.Sprintf("%s::%s", ns.Parent().Name(fullName), ns.name)
}

func (ns *Namespace) Parent() Entity {
	return ns.parent
}

func (ns *Namespace) Header(tabSize uint8) []string {
	header := make([]string, 0)
	indent := strings.Repeat(" ", int(tabSize))
	if ns.name == "" {
		header = append(header, "namespace")
	} else {
		header = append(header, fmt.Sprintf("namespace %s", ns.name))
	}
	header = append(header, "{")
	for n, c := range ns.children {
		if n != 0 {
			header = append(header, "")
		}
		for _, l := range c.Header(tabSize) {
			header = append(header, fmt.Sprintf("%s%s", indent, l))
		}
	}
	header = append(header, "}")
	return header
}

func (ns *Namespace) Source(tabSize uint8) []string {
	source := make([]string, 0)
	for _, c := range ns.children {
		source = append(source, c.Source(tabSize)...)
	}

	return source
}

func (ns *Namespace) SetParent(parent Entity) {
	ns.parent = parent
}

func (ns *Namespace) AddChildren(children ...Entity) *Namespace {
	for _, c := range children {
		c.SetParent(ns)
		ns.children = append(ns.children, c)
	}
	return ns
}

func (ns *Namespace) CreateNamespace(name string) *Namespace {
	return CreateNamespaceWithParent(name, ns)
}
