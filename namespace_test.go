package cpp

import "testing"
import "reflect"

func TestCreateNamespace(t *testing.T) {
	ns := CreateNamespace("")
	if ns.Name(false) != "" {
		t.Errorf("ns.Name(false) = '%s' != ''", ns.Name(false))
	}
	if ns.Name(true) != "" {
		t.Errorf("ns.Name(true) = '%s' != ''", ns.Name(true))
	}
	if ns.Parent() != nil {
		t.Error("ns.Parent() = %s != nil", ns.Parent())
	}
}

func TestHeaderForEmptyNamespace(t *testing.T) {
	ns := CreateNamespace("")
	if !reflect.DeepEqual(ns.Header(4), []string{"namespace", "{", "}"}) {
		t.Errorf("ns.Header(4) = '%s' != ['namespace','{','}']", ns.Header(4))
	}
}

func TestAppendNamespace(t *testing.T) {
	ns := CreateNamespace("unit").CreateNamespace("test").CreateNamespace("name_space")
	if ns.Parent() == nil {
		t.Error("ns.Parent() == nil")
	}
	if ns.Name(false) != "name_space" {
		t.Errorf("ns.Name(false) = '%s' != 'name_sapace'", ns.Name(false))
	}
	if ns.Name(true) != "unit::test::name_space" {
		t.Errorf("ns.Name(true) = '%s' != 'unit::test::name_sapace'", ns.Name(true))
	}

	entity := ns.Parent()
	if entity.Parent() == nil {
		t.Error("entity.Parent() = %s == nil", entity.Parent())
	}
	if entity.Name(false) != "test" {
		t.Errorf("entity.Name(false) = '%s' != 'test'", entity.Name(false))
	}
	if entity.Name(true) != "unit::test" {
		t.Errorf("entity.Name(true) = '%s' != 'unit::test'", entity.Name(false))
	}

	entity = entity.Parent()
	if entity.Parent() != nil {
		t.Error("entity.Parent() = %s != nil", entity.Parent())
	}
	if entity.Name(false) != "unit" {
		t.Errorf("entity.Name(false) = '%s' != 'unit'", entity.Name(false))
	}
	if entity.Name(true) != "unit" {
		t.Errorf("entity.Name(true) = '%s' != 'unit'", entity.Name(true))
	}
}

func TestHeaderForNestedNamespaces(t *testing.T) {
	ns := CreateNamespace("unit").AddChildren(CreateNamespace("test").AddChildren(CreateNamespace("name_space")))
	expected := []string{
		"namespace unit",
		"{",
		"    namespace test",
		"    {",
		"        namespace name_space",
		"        {",
		"        }",
		"    }",
		"}",
	}
	actual := ns.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("ns.Header(4) = '%s' != %v", actual, expected)
	}
}

func TestSpurceForNestedNamespaces(t *testing.T) {
	ns := CreateNamespace("unit").AddChildren(CreateNamespace("test").AddChildren(
		CreateMethod("TestMethod").Return(StringType).Code(
			Line("return std::wstring( L\"Hello world!\" );"),
		)))
	expected := []string{
		"std::wstring unit::test::TestMethod()",
		"{",
		"    return std::wstring( L\"Hello world!\" );",
		"}",
	}
	actual := ns.Source(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("ns.Spurce(4) = '%s' != %v", actual, expected)
	}
}
