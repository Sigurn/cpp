package cpp

import "testing"

func TestArgument_String(t *testing.T) {
	tests := []struct {
		name       string
		arg        Argument
		wantHeader string
		wantSource string
	}{
		{name: "Empty Argument", arg: CreateArgument("", nil), wantHeader: "", wantSource: ""},
		{name: "Argument without value", arg: CreateArgument("value", Int32Type), wantHeader: "int32_t value", wantSource: "int32_t value"},
		{name: "Argument with value", arg: CreateArgumentWithDefaultValue("value", Int32Type, Int32(315)), wantHeader: "int32_t value = 315", wantSource: "int32_t value"}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.arg.HeaderString(); got != tt.wantHeader {
				t.Errorf("Argument.HeaderString() = %v, wantHeader %v", got, tt.wantHeader)
			}
			if got := tt.arg.SourceString(); got != tt.wantSource {
				t.Errorf("Argument.SourceString() = %v, wantSource %v", got, tt.wantSource)
			}
		})
	}
}
