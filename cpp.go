package cpp

import (
	"bytes"
	"io"
	"strings"
)

type SourceFile struct {
	content    []string
	lineEnding string
}

func CreateSourceFile() *SourceFile {
	return &SourceFile{make([]string, 0), "\n"}
}

func (s *SourceFile) LineEnding(ending string) *SourceFile {
	s.lineEnding = ending
	return s
}

func (s *SourceFile) Lines(lines ...string) *SourceFile {
	s.content = append(s.content, lines...)
	return s
}

func (s *SourceFile) Write(writer io.Writer) error {
	buffer := bytes.NewBuffer([]byte{})
	_, err := buffer.WriteString(strings.Join(s.content, s.lineEnding))
	if err != nil {
		return err
	}
	_, err = writer.Write(buffer.Bytes())
	return err
}
