package cpp

import (
	"bytes"
	"fmt"
	"strings"
)

type InitField struct {
	name  string
	value Value
}

func CreateInitField(name string, value Value) InitField {
	return InitField{name: name, value: value}
}

type Constructor struct {
	args     []Argument
	initList []InitField
	parent   Entity
	explicit bool
	code     *CodeScope
}

func CreateConstructor(args ...Argument) *Constructor {
	return &Constructor{args: args, code: Scope("")}
}

func (ctor *Constructor) Name(fullName bool) string {
	if ctor.Parent() == nil {
		return ""
	}
	if !fullName {
		return ctor.Parent().Name(false)
	}
	return fmt.Sprintf("%s::%s", ctor.Parent().Name(true), ctor.Parent().Name(false))
}

func (ctor *Constructor) Parent() Entity {
	return ctor.parent
}

func (ctor *Constructor) Header(tabSize uint8) []string {
	var buffer bytes.Buffer
	if len(ctor.args) == 1 || ctor.explicit {
		buffer.WriteString(fmt.Sprintf("explicit %s", ctor.Name(false)))
	} else {
		buffer.WriteString(fmt.Sprintf("%s", ctor.Name(false)))
	}
	buffer.WriteString("(")
	var args []string
	for _, arg := range ctor.args {
		args = append(args, arg.String())
	}
	buffer.WriteString(strings.Join(args, ", "))
	buffer.WriteString(");")

	return []string{buffer.String()}
}

func (ctor *Constructor) Source(tabSize uint8) []string {
	indent := strings.Repeat(" ", int(tabSize))
	var buffer bytes.Buffer
	buffer.WriteString(ctor.Name(true))
	buffer.WriteString("(")
	var args []string
	for _, arg := range ctor.args {
		args = append(args, arg.SourceString())
	}
	buffer.WriteString(strings.Join(args, ", "))
	buffer.WriteString(")")
	source := make([]string, 0)
	source = append(source, buffer.String())
	for n, item := range ctor.initList {
		if n == 0 {
			source = append(source, fmt.Sprintf("%s: %s(%s)", indent, item.name, item.value))
		} else {
			source = append(source, fmt.Sprintf("%s, %s(%s)", indent, item.name, item.value))
		}
	}
	source = append(source, ctor.code.Source(tabSize)...)
	return source
}

func (ctor *Constructor) SetParent(parent Entity) {
	ctor.parent = parent
}

func (ctor *Constructor) Explicit(explicit bool) *Constructor {
	ctor.explicit = explicit
	return ctor
}

func (ctor *Constructor) Arguments(args ...Argument) *Constructor {
	ctor.args = append(ctor.args, args...)
	return ctor
}

func (ctor *Constructor) Init(fields ...InitField) *Constructor {
	ctor.initList = append(ctor.initList, fields...)
	return ctor
}

func (ctor *Constructor) Code(sources ...CodeEntity) *Constructor {
	ctor.code.Code(sources...)
	return ctor
}
