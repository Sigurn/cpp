package cpp

import (
	"fmt"
)

type Field struct {
	name         string
	fieldType    Type
	parent       Entity
	defaultValue Value
}

func CreateField(name string, fieldType Type) *Field {
	return &Field{name: name, fieldType: fieldType, defaultValue: fieldType.DefaultValue()}
}

func (f *Field) Name(fullName bool) string {
	if !fullName || f.Parent() == nil {
		return f.name
	}
	return fmt.Sprintf("%s::%s", f.Parent().Name(fullName), f.name)
}

func (f *Field) Parent() Entity {
	return f.parent
}

func (f *Field) Header(tabSize uint8) []string {
	return []string{fmt.Sprintf("%s %s;", f.fieldType, f.Name(false))}
}

func (f *Field) Source(tabSize uint8) []string {
	return nil
}

func (f *Field) SetParent(parent Entity) {
	f.parent = parent
}

func (f *Field) SetDefaultValue(defaultValue Value) *Field {
	f.defaultValue = defaultValue
	return f
}

func (f *Field) DefaultValue() Value {
	return f.defaultValue
}

func (f *Field) Type() Type {
	return f.fieldType
}
