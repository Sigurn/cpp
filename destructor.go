package cpp

import (
	"fmt"
)

type Destructor struct {
	parent  Entity
	virtual bool
	code    *CodeScope
}

func CreateDestructor() *Destructor {
	return &Destructor{code: Scope("")}
}

func (dtor *Destructor) Name(fullName bool) string {
	if dtor.Parent() == nil {
		return ""
	}
	if !fullName {
		return dtor.Parent().Name(false)
	}
	return fmt.Sprintf("%s::~%s", dtor.Parent().Name(true), dtor.Parent().Name(false))
}

func (dtor *Destructor) Parent() Entity {
	return dtor.parent
}

func (dtor *Destructor) Header(tabSize uint8) []string {
	if dtor.virtual {
		return []string{fmt.Sprintf("virtual ~%s();", dtor.Name(false))}
	}

	return []string{fmt.Sprintf("~%s();", dtor.Name(false))}
}

func (dtor *Destructor) Source(tabSize uint8) []string {
	source := make([]string, 0)
	source = append(source, fmt.Sprintf("%s()", dtor.Name(true)))
	source = append(source, dtor.code.Source(tabSize)...)
	return source
}

func (dtor *Destructor) SetParent(parent Entity) {
	dtor.parent = parent
}

func (dtor *Destructor) Virtual(virtual bool) *Destructor {
	dtor.virtual = virtual
	return dtor
}

func (dtor *Destructor) Code(sources ...CodeEntity) *Destructor {
	dtor.code.Code(sources...)
	return dtor
}
