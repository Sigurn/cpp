package cpp

import (
	"fmt"
	"strings"
)

type CodeLine struct {
	string
}

func Line(code string) *CodeLine {
	return &CodeLine{code}
}

func (cl *CodeLine) Source(tabSize uint8) []string {
	return []string{cl.string}
}

type CodeScope struct {
	title string
	code  []CodeEntity
}

func Scope(name string) *CodeScope {
	return &CodeScope{title: name}
}

func (cs *CodeScope) Code(entities ...CodeEntity) *CodeScope {
	cs.code = append(cs.code, entities...)
	return cs
}

func (cs *CodeScope) Source(tabSize uint8) []string {
	indent := strings.Repeat(" ", int(tabSize))
	source := make([]string, 0)
	if cs.title != "" {
		source = append(source, string(cs.title))
	}
	source = append(source, "{")
	for _, c := range cs.code {
		for _, e := range c.Source(tabSize) {
			source = append(source, fmt.Sprintf("%s%s", indent, e))
		}
	}
	source = append(source, "}")
	return source
}
