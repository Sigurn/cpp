package cpp

import (
	"reflect"
	"testing"
)

func TestConstModifier(t *testing.T) {
	tests := []struct {
		name string
		args Type
		want string
	}{
		{"UInt8", UInt8Type, "const uint8_t"},
		{"String", StringType, "const std::wstring"},
		{"Int64", Int64Type, "const int64_t"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Const(tt.args).Name(true); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Const().Name(true) = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRefModifier(t *testing.T) {
	tests := []struct {
		name string
		args Type
		want string
	}{
		{"UInt16", UInt16Type, "uint16_t&"},
		{"Float", FloatType, "float&"},
		{"Double", DoubleType, "double&"},
		{"String", StringType, "std::wstring&"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Ref(tt.args).Name(true); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ref().Name(true) = %v, want %v", got, tt.want)
			}
		})
	}
}


func TestConstRefModifier(t *testing.T) {
	tests := []struct {
		name string
		args Type
		want string
	}{
		{"Bool", BoolType, "const bool&"},
		{"String", StringType, "const std::wstring&"},
		{"Int32", Int32Type, "const int32_t&"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ConstRef(tt.args).Name(true); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ConstRef().Name(true) = %v, want %v", got, tt.want)
			}
		})
	}
}
