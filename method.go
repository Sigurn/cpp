package cpp

import (
	"bytes"
	"fmt"
	"strings"
)

type Method struct {
	name              string
	declParams        []Argument
	useParams         []Type
	ret               Type
	args              []Argument
	isVirtual         bool
	isStatic          bool
	isConst           bool
	isPureVirtual     bool
	isTemplate        bool
	callingConvention string
	parent            Entity
	code              *CodeScope
}

func CreateMethod(name string) *Method {
	return CreateMethodWithRetTypeAndArgs(name, VoidType)
}

func CreateMethodWithRetTypeAndArgs(name string, retType Type, args ...Argument) *Method {
	return &Method{name: name, ret: retType, args: args, code: Scope("")}
}

func (m *Method) IsTemplate() bool {
	return m.isTemplate || len(m.declParams) != 0 || len(m.useParams) != 0
}

func (m *Method) Name(fullName bool) string {
	if !fullName || m.Parent() == nil {
		return m.name
	}
	return fmt.Sprintf("%s::%s", m.Parent().Name(fullName), m.name)
}

func (m *Method) Parent() Entity {
	return m.parent
}

func (m *Method) getTemplateParams() []string {
	header := make([]string, 0)

	if !m.isTemplate && len(m.declParams) == 0 {
		return header
	}

	var buffer bytes.Buffer
	buffer.WriteString("template<")
	var params []string
	for _, param := range m.declParams {
		params = append(params, param.String())
	}
	buffer.WriteString(strings.Join(params, ", "))
	buffer.WriteString(">")
	return append(header, buffer.String())
}

func (m *Method) getUseParamsString() string {
	if !m.isTemplate || len(m.useParams) == 0 {
		return ""
	}

	var buffer bytes.Buffer

	buffer.WriteString("<")
	for n, param := range m.useParams {
		if n != 0 {
			buffer.WriteString(", ")
		}
		buffer.WriteString(param.Name(true))
	}
	buffer.WriteString(">")

	return buffer.String()
}

func (m *Method) Header(tabSize uint8) []string {
	_, isMember := m.parent.(*MemberGroup)

	header := m.getTemplateParams()
	var buffer bytes.Buffer

	if isMember && m.isStatic {
		buffer.WriteString("static ")
	} else if isMember && (m.isVirtual || m.isPureVirtual) {
		buffer.WriteString("virtual ")
	}
	buffer.WriteString(fmt.Sprintf("%s ", m.ret.Name(true)))
	if m.callingConvention != "" {
		buffer.WriteString(fmt.Sprintf("%s ", m.callingConvention))
	}
	buffer.WriteString(m.Name(false))
	buffer.WriteString(m.getUseParamsString())
	buffer.WriteString("(")
	var args []string
	for _, arg := range m.args {
		args = append(args, arg.HeaderString())
	}
	buffer.WriteString(strings.Join(args, ", "))
	buffer.WriteString(")")
	if isMember && m.isConst && !m.isStatic {
		buffer.WriteString(" const")
	}
	if isMember && m.isPureVirtual && !m.isStatic {
		buffer.WriteString(" = 0")
	}

	if len(m.declParams) != 0 {
		header = append(header, buffer.String())
		header = append(header, m.code.Source(tabSize)...)
	} else {
		buffer.WriteString(";")
		header = append(header, buffer.String())
	}

	return header
}

func (m *Method) Source(tabSize uint8) []string {
	_, isMember := m.parent.(*MemberGroup)

	header := make([]string, 0)
	header = append(header, m.getTemplateParams()...)
	var buffer bytes.Buffer

	buffer.WriteString(fmt.Sprintf("%s ", m.ret.Name(true)))
	buffer.WriteString(m.Name(true))
	buffer.WriteString(m.getUseParamsString())
	buffer.WriteString("(")
	var args []string
	for _, arg := range m.args {
		args = append(args, arg.SourceString())
	}
	buffer.WriteString(strings.Join(args, ", "))
	buffer.WriteString(")")
	if isMember && m.isConst && !m.isStatic {
		buffer.WriteString(" const")
	}
	header = append(header, buffer.String())
	header = append(header, m.code.Source(tabSize)...)
	return header
}

func (m *Method) SetParent(parent Entity) {
	m.parent = parent
}

func (m *Method) Return(retType Type) *Method {
	m.ret = retType
	return m
}

func (m *Method) Arguments(args ...Argument) *Method {
	m.args = append(m.args, args...)
	return m
}

func (m *Method) DeclareParameters(params ...Argument) *Method {
	m.declParams = append(m.declParams, params...)
	return m
}

func (m *Method) UseParameters(params ...Type) *Method {
	m.useParams = append(m.useParams, params...)
	return m
}

func (m *Method) Static(value bool) *Method {
	m.isStatic = value
	return m
}

func (m *Method) Const(value bool) *Method {
	m.isConst = value
	return m
}

func (m *Method) Virtual(value bool) *Method {
	m.isVirtual = value
	return m
}

func (m *Method) PureVirtual(value bool) *Method {
	m.isPureVirtual = value
	return m
}

func (m *Method) Template(value bool) *Method {
	m.isTemplate = value
	return m
}

func (m *Method) CallingConvention(callingConvention string) *Method {
	m.callingConvention = callingConvention
	return m
}

func (m *Method) Code(sources ...CodeEntity) *Method {
	m.code.Code(sources...)
	return m
}
