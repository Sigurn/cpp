package cpp

import "fmt"

type Argument struct {
	argType  Type
	argName  string
	defValue Value
}

func CreateArgument(name string, argType Type) Argument {
	return CreateArgumentWithDefaultValue(name, argType, nil)
}

func CreateArgumentWithDefaultValue(name string, argType Type, defValue Value) Argument {
	return Argument{argName: name, argType: argType, defValue: defValue}
}

func (arg Argument) String() string {
	return arg.HeaderString()
}

func (arg Argument) HeaderString() string {
	if arg.argType == nil {
		return ""
	} else if arg.argName == "" {
		return fmt.Sprintf("%s", arg.argType)
	} else if arg.defValue == nil {
		return fmt.Sprintf("%s %s", arg.argType, arg.argName)
	}
	return fmt.Sprintf("%s %s = %s", arg.argType, arg.argName, arg.defValue)
}

func (arg Argument) SourceString() string {
	if arg.argType == nil {
		return ""
	} else if arg.argName == "" {
		return fmt.Sprintf("%s", arg.argType)
	}
	return fmt.Sprintf("%s %s", arg.argType, arg.argName)
}
