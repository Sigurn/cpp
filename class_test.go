package cpp

import (
	"reflect"
	"testing"
)

func TestCreateClass(t *testing.T) {
	class := &Class{}
	name := class.Name(true)
	if name != "" {
		t.Errorf("class.Name(true) = %v != ''", name)
	}
	parent := class.Parent()
	if parent != nil {
		t.Errorf("class.Parent() = %v != nil", parent)
	}
	headerExpected := []string{"class", "{", "};"}
	headerActual := class.Header(4)
	if !reflect.DeepEqual(headerActual, headerExpected) {
		t.Errorf("class.Header(4) = %v != %v", headerActual, headerExpected)
	}
	source := class.Source(4)
	if !reflect.DeepEqual(source, []string{}) {
		t.Errorf("class.Source(4) = %v != []string", source)
	}
}

func TestClassHeaderInheritance(t *testing.T) {
	class := CreateClass("TestClass").
		Inherit(Public, CreateType("PublicClass1")).
		Inherit(Public, CreateType("PublicClass2")).
		Inherit(Protected, CreateType("ProtectedClass")).
		Inherit(Private, CreateType("PrivateClass"))
	expected := []string{
		"class TestClass",
		"    : public PublicClass1",
		"    , public PublicClass2",
		"    , protected ProtectedClass",
		"    , private PrivateClass",
		"{",
		"};",
	}
	actual := class.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("class.Header(4) = %v != %v", actual, expected)
	}
}

func TestClassHeaderTypes(t *testing.T) {
	class := CreateClass("TestClass").
		Private(CreateEnum("TestEnum").AddItem("Item1").AddItem("Item2").AddItem("Item3")).
		Protected(CreateClass("InternalClass")).
		Public(CreateClass("PublicClass").Inherit(Private, CreateType("IUnknown")))
	expected := []string{
		"class TestClass",
		"{",
		"private:",
		"    enum class TestEnum",
		"    {",
		"        Item1,",
		"        Item2,",
		"        Item3,",
		"    };",
		"",
		"protected:",
		"    class InternalClass",
		"    {",
		"    };",
		"",
		"public:",
		"    class PublicClass",
		"        : private IUnknown",
		"    {",
		"    };",
		"};",
	}
	actual := class.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("class.Header(4) = %v != %v", actual, expected)
	}
}

func TestClassHeaderFields(t *testing.T) {
	class := CreateClass("TestClass").
		Public(CreateField("m_intField", Int32Type), CreateField("m_boolField", BoolType)).
		Protected(CreateField("m_strField", StringType)).
		Private(CreateField("m_dblField", DoubleType), CreateField("m_i8Field", Int8Type))
	expected := []string{
		"class TestClass",
		"{",
		"public:",
		"    int32_t m_intField;",
		"    bool m_boolField;",
		"",
		"protected:",
		"    std::wstring m_strField;",
		"",
		"private:",
		"    double m_dblField;",
		"    int8_t m_i8Field;",
		"};",
	}
	actual := class.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("class.Header(4) = %v != %v", actual, expected)
	}
}

func TestClassHeaderMethods(t *testing.T) {
	class := CreateClass("TestClass").
		Public(
			CreateMethod("TestMethod1").
				Code(Line("throw std::exception(\"not implemented\");"))).
		Protected(
			CreateMethod("TestMethod2").PureVirtual(true).
				Code(Line("throw std::exception(\"not implemented\");"))).
		Private(
			CreateMethod("TestMethod3").Return(BoolType).Arguments(CreateArgument("flag", BoolType)).
				Code(Line("throw std::exception(\"not implemented\");")))
	expected := []string{
		"class TestClass",
		"{",
		"public:",
		"    void TestMethod1();",
		"",
		"protected:",
		"    virtual void TestMethod2() = 0;",
		"",
		"private:",
		"    bool TestMethod3(bool flag);",
		"};",
	}
	actual := class.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("class.Header(4) = %v != %v", actual, expected)
	}
}

func TestClassHeaderConstructor(t *testing.T) {
	class := CreateClass("TestClass").
		Public(
			CreateConstructor(CreateArgument("str", StringType))).
		Protected(
			CreateConstructor(CreateArgument("value", CreateType("const TestClass&")))).
		Private(
			CreateConstructor())
	expected := []string{
		"class TestClass",
		"{",
		"public:",
		"    explicit TestClass(std::wstring str);",
		"",
		"protected:",
		"    explicit TestClass(const TestClass& value);",
		"",
		"private:",
		"    TestClass();",
		"};",
	}
	actual := class.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("class.Header(4) = %v != %v", actual, expected)
	}
}

func TestClassHeaderTemplateMethods(t *testing.T) {
	class := CreateClass("TestClass").
		Private(
			CreateMethod("Method1").DeclareParameters(CreateArgument("T", CreateType("typename"))).Return(CreateType("T")).
				Code(
					Line("throw std::exception(\"Not implemented\");"),
				))
	expected := []string{
		"class TestClass",
		"{",
		"private:",
		"    template<typename T>",
		"    T Method1()",
		"    {",
		"        throw std::exception(\"Not implemented\");",
		"    }",
		"};",
	}
	actual := class.Header(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("class.Header(4) = %v != %v", actual, expected)
	}
}

func TestClassSource(t *testing.T) {
	class := CreateClass("TestClass").
		Public(
			CreateConstructor(CreateArgument("str", StringType)).
				Init(CreateInitField("m_name", CreateValue("str"))),
		).
		Protected(
			CreateConstructor(CreateArgument("value", CreateType("const TestClass&"))).
				Init(CreateInitField("m_name", CreateValue("value.m_name"))),
		).
		Private(
			CreateConstructor().
				Code(
					Line("m_name = L\"\";"),
				),
			CreateMethod("Method1").DeclareParameters(CreateArgument("T", CreateType("typename"))).Return(CreateType("T")).
				Code(
					Line("throw std::exception(\"Not implemented\");"),
				),
			CreateMethod("Method2").Return(StringType).Arguments(CreateArgument("name", ConstRef(StringType))).
				Code(
					Line("const std::wstring oldName = m_name;"),
					Line("m_name = name;"),
					Line("return oldName;"),
				),
		)
	expected := []string{
		"TestClass::TestClass(std::wstring str)",
		"    : m_name(str)",
		"{",
		"}",
		"",
		"",
		"TestClass::TestClass(const TestClass& value)",
		"    : m_name(value.m_name)",
		"{",
		"}",
		"",
		"",
		"TestClass::TestClass()",
		"{",
		"    m_name = L\"\";",
		"}",
		"",
		"",
		"std::wstring TestClass::Method2(const std::wstring& name)",
		"{",
		"    const std::wstring oldName = m_name;",
		"    m_name = name;",
		"    return oldName;",
		"}",
	}
	actual := class.Source(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("class.Source(4) = %v != %v", actual, expected)
	}
}
