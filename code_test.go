package cpp

import (
	"reflect"
	"testing"
)

func TestScopeWithTitle(t *testing.T) {
	scope := Scope("for (int i=0; i<100; i++)").Code(
		Line("cout << i << endl;"),
	)
	expected := []string{
		"for (int i=0; i<100; i++)",
		"{",
		"    cout << i << endl;",
		"}",
	}
	actual := scope.Source(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("scope.Source(4) = %v != %v", actual, expected)
	}
}

func TestScopeWithoutTitle(t *testing.T) {
	scope := Scope("").Code(
		Line("cout << \"hello world!\" << endl;"),
	)
	expected := []string{
		"{",
		"    cout << \"hello world!\" << endl;",
		"}",
	}
	actual := scope.Source(4)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("scope.Source(4) = %v != %v", actual, expected)
	}
}
